from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

# Sample data for the user profile
user_profile = {
    'username': 'jiro.ybanez',
    'email': 'jiroivancybanez@iskolarngbayan.pup.edu.ph',
    'bio': 'Tired Computer Engineering Student',
}

@app.route('/')
def index():
    return render_template('index.html', profile=user_profile)

@app.route('/edit', methods=['GET', 'POST'])
def edit():
    if request.method == 'POST':
        # Update the user profile with the form data
        user_profile['username'] = request.form['username']
        user_profile['email'] = request.form['email']
        user_profile['bio'] = request.form['bio']
        return redirect(url_for('index'))
    return render_template('edit.html', profile=user_profile)

if __name__ == '__main__':
    app.run(debug=True)
